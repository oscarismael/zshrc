export ANDROID_HOME=/Users/oscar/Library/Android/sdk
export PATH=$ANDROID_HOME/platform-tools:$PATH
export PATH=$ANDROID_HOME/tools:$PATH
export PATH=$ANDROID_HOME/tools/bin:$PATH
# try 208(orange) and 226(yellow) colors
#PROMPT='%F{049}%~%f -> '
alias web="cd ~/WebDevProjects/belladev"
# Load version control information
autoload -Uz vcs_info
precmd() { vcs_info }

# Format the vcs_info_msg_0_ variable
zstyle ':vcs_info:git:*' formats '(%b)'
 
# Set up the prompt (with git branch name)
setopt PROMPT_SUBST
PROMPT='%F{049}${PWD/#$HOME/~}%f %F{226}${vcs_info_msg_0_}%f-> '

